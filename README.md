# givebutter_listeners_vencabot
## Description
This module extends the streambrain_vencabot package with a 'Listener' which creates an Event whenever a GiveButter campaign (at a supplied API key) gets a new donation.

## Status
There's only one Listener for now. I will want to add more functionality in the future.

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
